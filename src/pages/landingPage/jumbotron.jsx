import React from 'react';
import 'bootstrap/dist/css/bootstrap.css'
import './jumbotron.css'

const Jumbotron = () => {
    return (
        <div class="bckgrnd">
            <div class="jumbotron jumbotron-fluid">
                <h1 class="title">L e t ' s  P l a y</h1>
                <button type="button" style={{color: '#313131'}} class="btn-jumbotron btn btn-success" >Book Now</button>
            </div>
        </div>
    )
}

export default Jumbotron
